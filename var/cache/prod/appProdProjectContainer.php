<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerXnxteah\appProdProjectContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerXnxteah/appProdProjectContainer.php') {
    touch(__DIR__.'/ContainerXnxteah.legacy');

    return;
}

if (!\class_exists(appProdProjectContainer::class, false)) {
    \class_alias(\ContainerXnxteah\appProdProjectContainer::class, appProdProjectContainer::class, false);
}

return new \ContainerXnxteah\appProdProjectContainer(array(
    'container.build_hash' => 'Xnxteah',
    'container.build_id' => 'f50ec343',
    'container.build_time' => 1519257791,
));
