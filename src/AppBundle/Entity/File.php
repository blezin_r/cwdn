<?php

namespace AppBundle\Entity;

/**
 * File
 */
class File
{
    /**
     * @var string
     */
    private $filename;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $Clesfile;

    /**
     * @var \AppBundle\Entity\Category
     */
    private $category;

    /**
     * @var \AppBundle\Entity\User
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->Clesfile = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set filename
     *
     * @param string $filename
     *
     * @return File
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add clesfile
     *
     * @param \AppBundle\Entity\Clefile $clesfile
     *
     * @return File
     */
    public function addClesfile(\AppBundle\Entity\Clefile $clesfile)
    {
        $this->Clesfile[] = $clesfile;

        return $this;
    }

    /**
     * Remove clesfile
     *
     * @param \AppBundle\Entity\Clefile $clesfile
     */
    public function removeClesfile(\AppBundle\Entity\Clefile $clesfile)
    {
        $this->Clesfile->removeElement($clesfile);
    }

    /**
     * Get clesfile
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClesfile()
    {
        return $this->Clesfile;
    }

    /**
     * Set category
     *
     * @param \AppBundle\Entity\Category $category
     *
     * @return File
     */
    public function setCategory(\AppBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \AppBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return File
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}

