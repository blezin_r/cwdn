<?php

namespace AppBundle\Entity;

/**
 * Clefile
 */
class Clefile
{
    /**
     * @var string
     */
    private $cles;

    /**
     * @var string
     */
    private $lang;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \AppBundle\Entity\File
     */
    private $file;


    /**
     * Set cles
     *
     * @param string $cles
     *
     * @return Clefile
     */
    public function setCles($cles)
    {
        $this->cles = $cles;

        return $this;
    }

    /**
     * Get cles
     *
     * @return string
     */
    public function getCles()
    {
        return $this->cles;
    }

    /**
     * Set lang
     *
     * @param string $lang
     *
     * @return Clefile
     */
    public function setLang($lang)
    {
        $this->lang = $lang;

        return $this;
    }

    /**
     * Get lang
     *
     * @return string
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file
     *
     * @param \AppBundle\Entity\File $file
     *
     * @return Clefile
     */
    public function setFile(\AppBundle\Entity\File $file = null)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return \AppBundle\Entity\File
     */
    public function getFile()
    {
        return $this->file;
    }
}

