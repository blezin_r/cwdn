/* 
Lorsque l'on clique sur un button avec la class 'comingSoon'
Creation du popup
*/
$('.comingSoon').on("click",function(event){
    event.preventDefault();
    $('body').append("<div id='overlay'></div>");
    $('#overlay').append('<div id="popup"></div>');
    $('#popup').append("<p>x</p><p>Coming Soon</p>");
});

/* 
Pour faire disparaitre le popup du 'Coming Soon'
*/
$(document).on('click','.closePopup, #overlay',function(){
    $("#overlay").remove();
});

/*
Lorsque le formulaire est charge et qu'il contient qu'une seul chose
alors l'aligment des elements s'effectue
(dans notre cas il s'agit juste de la confirmation de connection)
*/
$("main#mainForm").promise().done(function(){
    if($(this).length == 1)
        $("main#mainForm p").css({"textAlign" : "center", "margin" : "100px"});
});

/*
Lorsque l'on est sur la page d'accueil et 
que l'on rentre son nickname
cela prend la value de l'input pour la transferer
sur la page de creation de compte via le localStorage
*/
$("#newUserName").on("submit", function(event){
    var monStorage = localStorage;
    var value = $(".form-control.form-control-lg").val();

    monStorage.setItem('username',value);
});

/* 
Attend pour voir si l'input du nickname de la page d'enregistrement
est charge et que le local storage contient bien la cle 'username'
dans ce cas je met la value de la cle username dans l'input nickname
*/
$('#fos_user_registration_form_username').promise().done(function(){
    var monStorage = localStorage;
    
    if(localStorage.username){
        $('#fos_user_registration_form_username').val(localStorage.username);
    }
});

/*
Faire apparaitre un formulaire
quand on appuie sur le btn
"add a file" de la page dashboard
*/

$('#addFile').on('click', function(event){
    event.preventDefault();
    var form = $('main#mainDashBoard div + form');
    $(this).replaceWith(form);
});

/*
* Verification si le fichier UPLOADER est bien
* un fichier .yml
*/

$('#form_fileInputs').on('change', function(){
    var filename = $(this).val();
    var extension = /[^.]+$/.exec(filename).pop();
    if (extension == 'xliff'){
        alert("Le xliff n\'est pas encore pris en compte mais bientot t\'inquiete");
        $(this).val("");
    } else if (extension != 'yml'){
        alert("L\'extension \"."+ extension +"\" choisie n\'est pas la bonne !\nNous voulons un fichier .yml");
        $(this).val("");
    }
})

/*
* Affichage des noms des fichiers sans les timestamps
*/
$("main#mainDashBoard").promise().done(function() {
    var nombTd = $("td").length;
    for (var i = 0; i < nombTd; i ++) {
        if (/.*-/.exec($('td')[i].innerHTML)) {
            $('td')[i].innerHTML = /.*-/.exec($('td')[i].innerHTML).pop().slice(0, -1);
        }
    }
});
if($('h2#changeName')[0]) {
    $('h2#changeName').promise().done(function () {
        $(this).text(/.*-/.exec($(this).text()).pop().slice(0, -1));
    });
}

/*
* Verification si l'utilisateur est sur de vouloir supprimer le fichier
*/

$("main#mainDashBoard td.redRemove a").on('click', function(event){
   var conf = confirm('Etes-vous sur de supprimer le fichier "' + $(this).parent().prev().prev().text() + '" ?');

   conf ? true : event.preventDefault();
});