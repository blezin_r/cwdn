<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;

use AppBundle\Entity\Category;
use AppBundle\Repository\CategoryRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Session\Session;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Entity\File;
use AppBundle\Entity\Clefile;
use Symfony\Component\Yaml\Yaml;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="appbundle_homepage")
     */
    public function indexAction(Request $request)
    {
        if($this->getUser())
        {
            $categories = $this->getDoctrine()
                                ->getRepository(Category::class)
                                ->findAll();
            return $this->render('@AppBundle/views/index.html.twig', array(
                'categories' => $categories,
            ));
        }
        return $this->render('@AppBundle/views/index.html.twig');
    }
    

    /**
     * @Route("/dashboard", name="appbundle_dashboard")
     */
    public function dashboardAction(Request $request)
    {
        if($this->getUser()) {
            $currentUser = $this->get('security.token_storage')
                ->getToken()
                ->getUser();

            $date = new \DateTime();
            $date = $date->format('U');

            $addFileForm = $this->createFormBuilder()
                                ->add('fileInputs', FileType::class)
                                ->add('category', EntityType::class, array(
                                    'class' => 'AppBundle:Category',
                                    'query_builder' => function(CategoryRepository $categoryRepository)
                                    {
                                        return $categoryRepository->createQueryBuilder('u')
                                                    ->orderBy('u.categoryname', 'DESC');
                                        },
                                    'choice_label' => "categoryname",
                                    'expanded' => true,
                                    ))
                                ->add('langue',ChoiceType::class, array(
                                    'choices' => array(
                                        'Francais' => 'Fr',
                                        'Anglais' => 'En',
                                        'Extraterrestre' => 'Ex',
                                    ),
                                ))
                                ->getForm();

            $addFileForm->handleRequest($request);


            //Si le fichier est bon alors je le push dans les table
            if ($addFileForm->isSubmitted() && $addFileForm->isValid()) {
                $filename = $request->files->get('form')['fileInputs']->getClientOriginalName();

                $filenameWithExtension = pathinfo($filename)['filename']."-".$date. '.' . pathinfo($filename)['extension'];
                $filename = pathinfo($filename)['filename']."-".$date;

                $request->files->get('form')['fileInputs']
                                ->move(
                                    $this->getParameter('translationfile_directory'),
                                    $filenameWithExtension
                                );

                $idCategory = $request->request->get('form')['category'];
                $lang = $request->request->get('form')['langue'];

                $fileParsing = Yaml::parseFile($this->getParameter('translationfile_directory') . '/' . $filenameWithExtension);

                $this->get('session')->set('fileParsing', $fileParsing);
                return $this->redirectToRoute('appbundle_addFile', array(
                        'filename' => $filename,
                        'idCategory' => $idCategory,
                        'langue' => $lang,
                    )
                );
            }


            //Recuperation des fichiers uploader par le user
            $filesUpload = $this->getDoctrine()
                ->getRepository(File::class)
                ->findBy([
                    'user' => $currentUser->getId(),
                ]);

            return $this->render('@AppBundle/views/dashboard.html.twig',
                array('form' => $addFileForm->createView(),
                    'filesUpload' => $filesUpload,
                ));
        } else {
            return $this->redirectToRoute('appbundle_homepage');
        }
    }


    /**
     * @Route("/addFile/{langue}/{idCategory}/{filename}", name="appbundle_addFile")
     */
    public function addFileAction(Request $request, $langue ,$idCategory , $filename)
    {
        //Creation du formulaire pour push le fichier
        $fileParsing = array_keys($this->get('session')->get('fileParsing'));

        $file = new File();

        $currentUser = $this->get('security.token_storage')
            ->getToken()
            ->getUser();

        $category = $this->getDoctrine()
                        ->getRepository(Category::class)
                        ->findBy([
                            'id' => $idCategory,
                            ]);

        $em = $this->getDoctrine()
                    ->getManager();

        $file->setFileName($filename);
        $file->setCategory($category[0]);
        $file->setUser($currentUser);

        foreach($fileParsing as $key) {
            $keysfile = new Clefile();
            $keysfile->setCles($key);
            $keysfile->setLang($langue);
            $em->persist($keysfile);
            $keysfile->setFile($file);
        }

        $em->persist($keysfile);
        $em->persist($file);
        $em->flush();

        return $this->redirectToRoute('appbundle_dashboard');
    }

    /**
     * @Route("/removeFile/{idFile}", name="appbundle_removeFile")
     */
    public function removeFileAction(Request $request, $idFile)
    {
        $em = $this->getDoctrine()
                    ->getManager();
        $fileDelete = $em->getRepository('AppBundle:File')
                        ->find($idFile);
        $em->remove($fileDelete);
        $em->flush();
        unlink($this->getParameter('translationfile_directory') . '/' . $fileDelete->getFilename() . '.yml');



        return $this->redirectToRoute('appbundle_dashboard');
    }

    /**
     * @Route("/categories/{idCategory}", name="appbundle_categories")
     */
    public function categoriesAction(Request $request, $idCategory)
    {
        if($this->getUser())
        {
            $filesUpload = $this->getDoctrine()
                ->getRepository(File::class)
                ->findBy([
                    'category' => $idCategory,
                ]);

            $category = $this->getDoctrine()
                ->getRepository(Category::class)
                ->findBy([
                    'id' => $idCategory,
                ]);

            $categoryname = $category[0]->getCategoryname();
            return $this->render('@AppBundle/views/categories.html.twig', array(
                "filesUpload" => $filesUpload,
                "categoryname" => $categoryname,
            ));
        }

        return $this->redirectToRoute('appbundle_homepage');
    }

    /**
     * @Route("/file/{idFile}", name="appbundle_fileContent")
     */
    public function fileContentAction(Request $request, $idFile){

        if($this->getUser()) {
            $fileContent = $this->getDoctrine()
                ->getRepository(Clefile::class)
                ->findBy([
                    'file' => $idFile,
                ]);
            return $this->render('@AppBundle/views/file.html.twig', array(
                'fileContent' => $fileContent,
            ));
        }
        return $this->redirectToRoute('appbundle_homepage');
    }
}
