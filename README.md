CWDN
=======

A Symfony project created on January 14, 2018, 12:51 am. for a school.

CWDN est un site de traduction communautaire.
L'inscription est gratuite.
Les membres du site peuvent demander à la communauté de traduire des fichiers i18n, ou proposer des traductions des fichiers i18n que d'autres utilisateurs auront proposées. On ne vous demandera pas de refaire tout leur site (on n'est pas si méchant en vrai), mais certaines fonctionnalités.


Cahier des charges techniques

**Technologie serveur**

-   Comme vous l'avez compris, le langage utilisé sera du PHP en version 7.0 ou 7.1
-   L'utilisation de Symfony est obligatoire en version LTS (si vous utilisez une autre version, vous devrez justifier pourquoi et avec de bons arguments sinon un malus vous aurez)
-   Server Apache ou Nginx
-   Mysql

**Technologie client**

-   HTML / CSS / Javascript
-   Conseil de framework / lib
-   JQuery
-   Underscore
-   Déconseil de framework / lib (ce n'est pas le but du projet et vous avez beaucoup de chose déjà)

-   React
-   Angular

**Contrainte Symfony(que)**

-   Le site devra être complétement disponible en plusieurs langues (vous devez choisir au moins une langue en plus du français, l'elfe est autorisé)
-   Vous devrez utiliser les bonnes pratiques (annotation, voter, ...)

**Contrainte globale**

-   Le site ne devra pas mettre plus de 5 sec à charger par page.