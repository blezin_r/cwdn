
$('.comingSoon').on("click",function(event){
    event.preventDefault();
    $('body').append("<div id='overlay'></div>");
    $('#overlay').append('<div id="popup"></div>');
    $('#popup').append("<p>x</p><p>Coming Soon Later</p>");
});

$("#newUserName").on("submit", function(event){
    var monStorage = localStorage;
    var value = $(".form-control.form-control-lg").val();

    monStorage.setItem('username',value);
});

$('#fos_user_registration_form_username').promise().done(function(){
    var monStorage = localStorage;
    
    if(localStorage.username){
        $('#fos_user_registration_form_username').val(localStorage.username);
    }
});

$(document).on('click','.closePopup, #overlay',function(){
    $("#overlay").remove();
});

$("main#mainForm").promise().done(function(){
    if($(this).length == 1)
        $("main#mainForm p").css({"textAlign" : "center", "margin" : "100px"});
});